# -*- coding: utf-8 -*-
from openerp import fields,models,api,_
from datetime import date
import openerp.addons.decimal_precision as dp


class c_claveprodserv (models.Model):

    _name='c.claveprodserv'

    code = fields.Char(string='ClaveProdServ')
    name = fields.Char(string='Descripcion')
    c_parent_id= fields.Many2one('c.claveprodserv', string='Parent')
    start_life = fields.Date(string='Inicio_Vigencia')
    end_life = fields.Date(string='Fin_Vigencia')
    iva= fields.Char(string='IVA_Traslado')
    ieps= fields.Char(string='IEPS_Traslado')
    complemento= fields.Char(string='Complemento')

