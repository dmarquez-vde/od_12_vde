# -*- coding: utf-8 -*-
###################################################################################
#
#    Vos Datos Enterprise Suite S.A. de C.V.
#    Copyright (C) 2018-TODAY VDE Suite(<https://www.vde-suite.com>).
#    Author: David Marquez dmarquez@vde-suite.com
#
#    This program is free software: you can modify
#    it under the terms of the GNU Affero General Public License (AGPL) as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###################################################################################
{
    'name': 'ZTK Devices Integration',
    'version': '12.0.1.0.0',
    'summary': """Integrating ZTKeco Biometric Devices """,
    'description': 'This module integrates Odoo with the biometric devices: ZKTeco',
    'category': 'Generic Modules/Human Resources',
    'author': 'VDE Suite',
    'company': 'VDE Suite',
    'website': "https://www.vde-suite.com",
    'depends': ['base_setup', 'hr_attendance'],
    'data': [
        'security/ir.model.access.csv',
        'views/zk_attendance_view.xml',
        'data/sync_data.xml'

    ],
    'images': [],
    'license': 'AGPL-3',
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': False,
}
