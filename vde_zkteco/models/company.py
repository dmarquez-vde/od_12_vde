# -*- coding: utf-8 -*-
###################################################################################
#
#    Vos Datos Enterprise Suite S.A. de C.V.
#    Copyright (C) 2018-TODAY VDE Suite(<https://www.vde-suite.com>).
#    Author: David Marquez dmarquez@vde-suite.com
#
#    This program is free software: you can modify
#    it under the terms of the GNU Affero General Public License (AGPL) as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###################################################################################
from odoo import tools
from odoo import models, fields, api, _
from collections import namedtuple
from odoo.exceptions import UserError
import datetime
import logging
from dateutil import tz

_logger = logging.getLogger(__name__)

class ResCompany(models.Model):
    _inherit = 'res.company'

    ips = fields.Many2many('res.company.ip', string='IPs permitidas')

class ResCompanyIp(models.Model):
    _name = 'res.company.ip'

    name = fields.Char(string="Address")

    
