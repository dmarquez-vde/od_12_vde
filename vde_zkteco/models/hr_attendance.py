# -*- coding: utf-8 -*-
###################################################################################
#
#    Vos Datos Enterprise Suite S.A. de C.V.
#    Copyright (C) 2018-TODAY VDE Suite(<https://www.vde-suite.com>).
#    Author: David Marquez dmarquez@vde-suite.com
#
#    This program is free software: you can modify
#    it under the terms of the GNU Affero General Public License (AGPL) as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###################################################################################
from odoo import tools
from odoo import models, fields, api, _
from collections import namedtuple
from odoo.exceptions import UserError
import datetime
import logging
from dateutil import tz

_logger = logging.getLogger(__name__)

class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    device_id = fields.Integer(string='Device ID')
    check_in = fields.Float(string='Hora Entrada', default=9)
    tolerance = fields.Float(string='Tolerancia', default=.25)
    check_out = fields.Float(string='Hora Salida', default=18)

class ZKAttendance(models.Model):
    _name = 'zk.attendance'

    employee_id = fields.Many2one('hr.employee', string="Empleado")
    name = fields.Date(string="Fecha")
    punching = fields.Datetime(string="Registro")
    punching_real = fields.Datetime(string="Registro Real")
    state = fields.Selection([('draft','Borrador'), ('done','Procesado')], string="Estatus", default='draft', copy=False)

    @api.model
    def cron_sync(self):
        attendances = self.env['zk.attendance'].search([('state','=','draft')])
        if attendances:
            attendances.sync_attendance(attendances)

    def convert_to_utc(self, fecha):
        to_zone = tz.gettz("UTC")
        from_zone = tz.gettz("America/Mexico_City")
        local = fecha.replace(tzinfo=from_zone)
        utc = local.astimezone(to_zone)
        return utc

    def convert_to_local(self, fecha):
        from_zone = tz.gettz("UTC")
        to_zone = tz.gettz("America/Mexico_City")
        utc = fecha.replace(tzinfo=from_zone)
        local = utc.astimezone(to_zone)
        return local

    @api.multi
    def sync_attendance(self, ids):
        _logger.info("++++++++++++Cron Executed++++++++++++++++++++++")
        #att_ids = self.env['zk.attendance'].browse(ids)
        print ("esto es idss ", ids)
        horarios = {}
        _Mapping = namedtuple('Mapping', ('name', 'user_id'))
        for line in ids:
            print ("esto es line ", line)
            punching = []
            rel = []
            key = _Mapping(line.name, line.employee_id.id)
            horarios.setdefault(key, {})
            if horarios[key]:
                rel.append(line.id)
                rel.append(line.punching)
                horarios[key].append(rel)
            else:
                rel.append(line.id)
                rel.append(line.punching)
                punching.append(rel)
                horarios[key] = punching
        print ("horarios", horarios)
        print ("len horarios ", len(horarios))
        for item in horarios:
            check_in_stat = ''
            check_out_stat = ''

            #print ("len item",len(horarios.get(item)))
            if len(horarios.get(item)) == 1:
                dt = datetime.datetime.today()
                for index in horarios.get(item):
                    print ("esto es index ", index)
                    unico = index[1]
                    unico = self.convert_to_local(unico)
                #unico = (index[1] for index in horarios.get(item))
                print ("unico ", unico)
                if unico.time() < datetime.time(14,00,00):
                    print ("es <")
                    menor = self.convert_to_utc(unico)
                    mayor = self.convert_to_utc(datetime.datetime(unico.year,unico.month,unico.day,18,00,00))
                    check_out_stat = 'no'
                else:
                    print ("es >")
                    menor = self.convert_to_utc(datetime.datetime(unico.year,unico.month,unico.day,10,10,10))
                    mayor = self.convert_to_utc(unico)
                    check_in_stat = 'no'
            else:
                print ("item=>>>", item)
                print ("getattr=>>>", horarios.get(item))
                print ("name=>>>", item[0])
                print ("user=>>>", item[1])
                mayor = max(index[1] for index in horarios.get(item))
                menor = min(index[1] for index in horarios.get(item))
            print ("Mayor=>>>", mayor)
            print ("Menor=>>>", menor)
            print ("Diferencia=>>>", menor.time())
            #tolerancia_in = datetime.time(9,15,00)
            emp = self.env['hr.employee'].browse(item[1])
            entrada = int(emp.check_in)
            tolerancia = int(60 * emp.tolerance)
            salida = int(emp.check_out)
            if salida == 24:
                salida = 0
            if entrada == 24:
                entrada = 0
            tolerancia_in = datetime.time(entrada,tolerancia,00)
            print ("Tolerancia=>>>", tolerancia_in)
            menor_local = self.convert_to_local(menor)
            if menor_local.time() < tolerancia_in:
                print ("La entrada es antes de tolerancia")
                check_in_status = 'ontime'
            else:
                print ("La entrada fue despues de la tolerancia")
                check_in_status = 'delay'
            tolerancia_out = datetime.time(salida,59,59)
            time_out = datetime.time(salida,00,00)
            mayor_local = self.convert_to_local(mayor)
            print ("Esto es local ", mayor_local.time())
            print ("Esto es time_out ", time_out)
            print ("Esto es tolerancia_out ", tolerancia_out)
            if mayor_local.time() < time_out:
                print ("La Salida es antes de tolerancia")
                check_out_status = 'before'
            elif mayor_local.time() > tolerancia_out:
                print ("La Salida es despues")
                check_out_status = 'after'
            elif mayor_local.time() < tolerancia_out:
                print ("La Salida es a tiempo")
                check_out_status = 'ontime'
            print ("check_in_stat ", check_in_stat)
            print ("check_out_stat ", check_out_stat)
            if check_in_stat == 'no':
                check_in_status = 'no'
            if check_out_stat == 'no':
                check_out_status = 'no'
            attendance_ids = self.env['hr.attendance'].search([('name','=',item[0]),('employee_id','=',item[1])])
            print ("attendance_ids =>>", attendance_ids)
            if not attendance_ids:
                print ("NO hay registros")
                att_id = self.env['hr.attendance'].create({'employee_id':item[1], 'name':item[0], 'check_in':menor, 'check_out':mayor, 'check_in_status': check_in_status, 'check_out_status': check_out_status})
                if att_id:
                    for registro in horarios.get(item):
                        print ("Actualizando =>>",self.env['zk.attendance'].browse(registro[0]).write({'state':'done'}))
            else:
                print ("YA existen registros")

class HrAttendance(models.Model):
    _inherit = 'hr.attendance'

    @api.constrains('check_in', 'check_out', 'employee_id')
    def _check_validity(self):
        """overriding the __check_validity function for employee attendance."""
        #pass
        return True
    name = fields.Date(string='Punching Date')
    department_id = fields.Many2one('hr.department', string="Departamento", related='employee_id.department_id')
    check_in_status = fields.Selection([('no','No checo'),('ontime','A tiempo'),('delay','Retardo')], string="Estado de entrada", copy=False)
    check_out_status = fields.Selection([('no','No checo'),('before','Antes'), ('ontime','A tiempo'),('after','Despues')], string="Estado de Salida", copy=False)



class ZkAttendanceWizardLine(models.TransientModel):
    _name = 'zk.attendance.wizard.line'

    wizard_id = fields.Many2one('zk.attendance.wizard')
    attendance_id = fields.Many2one('zk.attendance')
    employee = fields.Char(related="attendance_id.employee_id.name", store=True)


class ZkAttendanceWizard(models.TransientModel):
    _name = 'zk.attendance.wizard'

    line_ids = fields.One2many('zk.attendance.wizard.line', 'wizard_id')


    @api.model
    def default_get(self,fields):
        res = super(ZkAttendanceWizard, self).default_get(fields)
        inv_ids = self._context.get('active_ids')
        vals=[]
        attendance_ids = self.env['zk.attendance'].browse(inv_ids)
        for attendance in attendance_ids:
            if not attendance.state == 'draft':
                raise UserError(_("Solo pueden procesarse registros en estado 'Borrador'"))
            else:
                vals.append((0,0,{'attendance_id':attendance.id,'employee':attendance.employee_id.name}))
        res.update({'line_ids':vals})
        return res

    def convert_to_utc(self, fecha):
        to_zone = tz.gettz("UTC")
        from_zone = tz.gettz("America/Mexico_City")
        local = fecha.replace(tzinfo=from_zone)
        utc = local.astimezone(to_zone)
        return utc

    def convert_to_local(self, fecha):
        from_zone = tz.gettz("UTC")
        to_zone = tz.gettz("America/Mexico_City")
        utc = fecha.replace(tzinfo=from_zone)
        local = utc.astimezone(to_zone)
        return local

    @api.multi
    def merge_attendance(self):
        horarios = {}
        _Mapping = namedtuple('Mapping', ('name', 'user_id'))
        for line in self.line_ids:
            punching = []
            rel = []
            key = _Mapping(line.attendance_id.name, line.attendance_id.employee_id.id)
            horarios.setdefault(key, {})
            if horarios[key]:
                rel.append(line.attendance_id.id)
                rel.append(line.attendance_id.punching)
                horarios[key].append(rel)
            else:
                rel.append(line.attendance_id.id)
                rel.append(line.attendance_id.punching)
                punching.append(rel)
                horarios[key] = punching
        print ("horarios", horarios)
        print ("len horarios ", len(horarios))
        for item in horarios:
            check_in_stat = ''
            check_out_stat = ''

            #print ("len item",len(horarios.get(item)))
            if len(horarios.get(item)) == 1:
                dt = datetime.datetime.today()
                for index in horarios.get(item):
                    print ("esto es index ", index)
                    unico = index[1]
                    unico = self.convert_to_local(unico)
                #unico = (index[1] for index in horarios.get(item))
                print ("unico ", unico)
                if unico.time() < datetime.time(14,00,00):
                    print ("es <")
                    menor = self.convert_to_utc(unico)
                    mayor = self.convert_to_utc(datetime.datetime(unico.year,unico.month,unico.day,18,00,00))
                    check_out_stat = 'no'
                else:
                    print ("es >")
                    menor = self.convert_to_utc(datetime.datetime(unico.year,unico.month,unico.day,10,10,10))
                    mayor = self.convert_to_utc(unico)
                    check_in_stat = 'no'
            else:
                print ("item=>>>", item)
                print ("getattr=>>>", horarios.get(item))
                print ("name=>>>", item[0])
                print ("user=>>>", item[1])
                mayor = max(index[1] for index in horarios.get(item))
                menor = min(index[1] for index in horarios.get(item))
            print ("Mayor=>>>", mayor)
            print ("Menor=>>>", menor)
            print ("Diferencia=>>>", menor.time())
            #tolerancia_in = datetime.time(9,15,00)
            emp = self.env['hr.employee'].browse(item[1])
            entrada = int(emp.check_in)
            tolerancia = int(60 * emp.tolerance)
            salida = int(emp.check_out)
            if salida == 24:
                salida = 0
            if entrada == 24:
                entrada = 0
            tolerancia_in = datetime.time(entrada,tolerancia,00)
            print ("Tolerancia=>>>", tolerancia_in)
            menor_local = self.convert_to_local(menor)
            if menor_local.time() < tolerancia_in:
                print ("La entrada es antes de tolerancia")
                check_in_status = 'ontime'
            else:
                print ("La entrada fue despues de la tolerancia")
                check_in_status = 'delay'
            tolerancia_out = datetime.time(salida,59,59)
            time_out = datetime.time(salida,00,00)
            mayor_local = self.convert_to_local(mayor)
            if mayor_local.time() < time_out:
                print ("La Salida es antes de tolerancia")
                check_out_status = 'before'
            elif mayor_local.time() > tolerancia_out:
                print ("La Salida es despues de tolerancia")
                check_out_status = 'after'
            elif mayor_local.time() < tolerancia_out:
                print ("La Salida es a tiempo")
                check_out_status = 'ontime'
            print ("check_in_stat ", check_in_stat)
            print ("check_out_stat ", check_out_stat)
            if check_in_stat == 'no':
                check_in_status = 'no'
            if check_out_stat == 'no':
                check_out_status = 'no'
            attendance_ids = self.env['hr.attendance'].search([('name','=',item[0]),('employee_id','=',item[1])])
            print ("attendance_ids =>>", attendance_ids)
            if not attendance_ids:
                print ("NO hay registros")
                att_id = self.env['hr.attendance'].create({'employee_id':item[1], 'name':item[0], 'check_in':menor, 'check_out':mayor, 'check_in_status': check_in_status, 'check_out_status': check_out_status})
                if att_id:
                    for registro in horarios.get(item):
                        print ("Actualizando =>>",self.env['zk.attendance'].browse(registro[0]).write({'state':'done'}))
            else:
                print ("YA existen registros")
