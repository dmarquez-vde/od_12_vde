from openerp import fields,models,api
from datetime import date, datetime, timedelta
#import datetime
import math
from openerp.exceptions import Warning
from openerp import tools
from openerp.osv import osv
from openerp.tools.translate import _
from odoo.exceptions import UserError, AccessError



class Employee(models.Model):
    _inherit = "hr.employee"

    date_join = fields.Date(string='Fecha Ingreso')


class hr_holidays(models.Model):
    _inherit='hr.leave'

    #date_from = fields.Date(string="Fecha inicio")
    #date_to = fields.Date(string="Fecha Fin")

    periodo = fields.Integer(string="Periodo")
    date_return = fields.Date(string="Fecha regreso")
    fecha_creado = fields.Date(string="Fecha creacion", default=datetime.now().date())
    motivo = fields.Selection([('v','Vacaciones'),('c','Compensatorios'),('e','Extraordinarios')], string="Motivo de ausencia", required=True)
    nota = fields.Text(string="Descripcion")



#    def _get_number_of_days(self, date_from, date_to, employee_id):
#        """ Returns a float equals to the timedelta between two dates given as string."""
#        from_dt = fields.Datetime.from_string(date_from)
#        to_dt = fields.Datetime.from_string(date_to)
#
#        if employee_id:
#            employee = self.env['hr.employee'].browse(employee_id)
#            resource = employee.resource_id.sudo()
#            if resource and resource.calendar_id:
#                hours = resource.calendar_id.get_working_hours(from_dt, to_dt, resource_id=resource.id, compute_leaves=True)
#                uom_hour = resource.calendar_id.uom_id
#                uom_day = self.env.ref('product.product_uom_day')
#                if uom_hour and uom_day:
#                    return uom_hour._compute_quantity(hours, uom_day)

        #time_delta = to_dt - from_dt
        #return math.ceil(time_delta.days + float(time_delta.seconds) / 86400)
        #fromdate = datetime.strptime(date_from,'%Y-%m-%d %H:%M:%S')
        #todate = datetime.strptime(date_to,'%Y-%m-%d %H:%M:%S')
        #fromdate = datetime.strptime(date_from,'%Y-%m-%d')
        #todate = datetime.strptime(date_to,'%Y-%m-%d')
        #daygenerator = (fromdate + timedelta(x + 1) for x in xrange((todate - fromdate).days))
        #diff_day=sum(1 for day in daygenerator if day.weekday() < 5)
        #return diff_day + 1

    @api.onchange('date_from', 'date_to')
    def onchange_dates(self):
        #result = {'value':{'number_of_days_temp':0}}
        if self.date_to:
            #todate=datetime.strptime(self.date_to,'%Y-%m-%d %H:%M:%S')
            #todate=datetime.strptime(self.date_to,'%Y-%m-%d')
            todate = self.date_to
            #fromdate=datetime.strptime(self.date_from,'%Y-%m-%d')
            fromdate = self.date_from
            #print "esto es todate ", todate
            todate =  todate.date()
            fromdate =  fromdate.date()
            #print ("esto es todate ", todate)
            #print ("esto es dia ",todate.weekday())
            #print ("esto es dia ",todate.weekday())
            if fromdate.weekday() == 5:
                raise UserError(_("No puedes seleccionar los dias Sabado") )
            if fromdate.weekday() == 6:
                raise UserError(_("No puedes seleccionar los dias Domingo") )
            if todate.weekday() == 5:
                raise UserError(_("No puedes seleccionar los dias Sabado") )
            if todate.weekday() == 6:
                raise UserError(_("No puedes seleccionar los dias Domingo") )
            h = 1
            ds = 5 - todate.weekday()
            s = 0
            if h >=ds:
                s = s + 2
                h = h -ds
            s = s + h / 5 * 2
            print ("esto es calculo ", date.fromordinal(todate.toordinal() + 1 + int(s) ))
            self.date_return=date.fromordinal(todate.toordinal() + 1 + int(s) )

    @api.onchange('employee_id')
    def onchange_employee(self):
        result = {'value': {'department_id': False, 'periodo': False}}
        if self.employee_id:
            employee = self.env['hr.employee'].browse(self.employee_id.id)
            if employee.date_join:
                date2 = str(employee.date_join).split('-')
                fecha_a_calcular = datetime.strptime(date2[0] + date2[1] + date2[2], '%Y%m%d').date()
                date1 = str(self.fecha_creado).split('-')
                fecha_a_calcular1 = datetime.strptime(date1[0] + date1[1] + date1[2], '%Y%m%d').date()
                diferencia = fecha_a_calcular1 - fecha_a_calcular
                #print "diferencia: ", diferencia.days / 365
                #res[holiday.id]=diferencia.days / 365
                result['value'] = {'department_id': employee.department_id.id, 'periodo':diferencia.days / 365}
            else:
                result['value'] = {'department_id': employee.department_id.id, 'periodo': 0}
        return result

    @api.multi
    def write(self, vals):
        if vals.get('date_from'):
            fromdate=datetime.strptime(vals.get('request_date_from'),'%Y-%m-%d')
            fromdate =  fromdate.date()
            if fromdate.weekday() == 5:
                raise UserError(_("No puedes seleccionar los dias Sabado") )
            if fromdate.weekday() == 6:
                raise UserError(_("No puedes seleccionar los dias Domingo") )
        if vals.get('date_to'):
            todate=datetime.strptime(vals.get('request_date_to'),'%Y-%m-%d')
            todate =  todate.date()
            if todate.weekday() == 5:
                raise UserError(_("No puedes seleccionar los dias Sabado") )
            if todate.weekday() == 6:
                raise UserError(_("No puedes seleccionar los dias Domingo") )
        result = super(hr_holidays, self).write(vals)
        return result

    @api.model
    def create(self,vals):
        if vals.get('date_from'):
            fromdate=datetime.strptime(vals.get('request_date_from'),'%Y-%m-%d')
            fromdate =  fromdate.date()
            if fromdate.weekday() == 5:
                raise UserError(_("No puedes seleccionar los dias Sabado") )
            if fromdate.weekday() == 6:
                raise UserError(_("No puedes seleccionar los dias Domingo") )
        if vals.get('date_to'):
            todate=datetime.strptime(vals.get('request_date_to'),'%Y-%m-%d')
            todate =  todate.date()
            if todate.weekday() == 5:
                raise UserError(_("No puedes seleccionar los dias Sabado") )
            if todate.weekday() == 6:
                raise UserError(_("No puedes seleccionar los dias Domingo") )
        sale_rec = super(hr_holidays,self).create(vals)
        return sale_rec
