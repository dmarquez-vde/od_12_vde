# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2010-Vos Datos Enterprise Suite SA de CV
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import json
from lxml import etree
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from odoo import api, fields, models, tools,  _
from odoo.tools import float_is_zero
from odoo.tools.misc import formatLang
from odoo.exceptions import UserError, RedirectWarning, ValidationError
import odoo.addons.decimal_precision as dp
from pytz import timezone

class ProjectSprint(models.Model):

    _name = 'project.sprint'

    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char(string="Folio")
    ref = fields.Char(string="Referencia")
    start_date = fields.Date(string="Fecha Inicio")
    end_date = fields.Date(string="Fecha Fin")
    project_id = fields.Many2one('project.project', string="Proyecto")
    pm_id = fields.Many2one('res.users', string="Project Manager")
    scrum_id = fields.Many2one('res.users', string="Scrum Master")
    planned_hours = fields.Float(string="Horas planeadas", compute="_get_hours")
    effective_hours = fields.Float(string="Horas efectivas", compute="_get_hours")
    task_ids = fields.One2many('project.task', 'sprint_id', string="Tareas")
    description = fields.Html(string="Descripcion")
    work_days = fields.Integer(string="Dias Habiles", compute="_get_work_days")
    work_hours = fields.Float(string="Horas Laborales", compute="_get_work_days")

    @api.multi
    @api.depends('task_ids')
    def _get_hours(self):
        planned = 0
        effective = 0
        for task in self.task_ids:
            planned = planned + task.planned_hours
            effective = effective + task.effective_hours
        self.planned_hours = planned
        self.effective_hours = effective

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('project.sprint')
        result = super(ProjectSprint, self).create(vals)
        return result

    @api.multi
    def _get_work_days(self):
        days = []
        excluded = [6,7]
        print ("excluded ", excluded)
        start = self.start_date
        end = self.end_date
        while start <= end:
            print ("days ", start.isoweekday())
            if start.isoweekday() not in excluded:
                days.append(start)
            start += timedelta(days=1)
        self.work_days = len(days)
        self.work_hours = len(days) * 5

    @api.multi
    def update_deadline(self):
        for task in self.task_ids:
            task.write({'date_deadline':self.end_date})
