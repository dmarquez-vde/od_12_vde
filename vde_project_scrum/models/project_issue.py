# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2010-Vos Datos Enterprise Suite SA de CV
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import json
from lxml import etree
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from odoo import api, fields, models, tools,  _
from odoo.tools import float_is_zero
from odoo.tools.misc import formatLang
from odoo.exceptions import UserError, RedirectWarning, ValidationError
import odoo.addons.decimal_precision as dp
from pytz import timezone

class ProjectIssue(models.Model):

    _name = 'project.issue'

    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char(string="Folio")
    ref = fields.Char(string="Referencia")
    date = fields.Date(string="Fecha Inicio",default=lambda self:datetime.now().date() or False)
    task_id = fields.Many2one('project.task', string="Tarea")
    project_id = fields.Many2one('project.project', string="Proyecto")
    user_id = fields.Many2one('res.users', string="Usuario")
    description = fields.Html(string="Descripcion")
    state=fields.Selection([('draft','Nueva'),
                               ('open','En proceso'),
                               ('cancel', 'Cancelada'),
                               #('pending','Pendiente'),
                               ('done','Cerrada')],
                              'Status', required=True, copy=False, default='draft')

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('project.issue')
        result = super(ProjectIssue, self).create(vals)
        return result

    @api.multi
    def set_done(self):
        return self.write({'state': 'done','color': 5})

    @api.multi
    def set_cancel(self):
        return self.write({'state': 'cancel', 'color':2})

    @api.multi
    def set_pending(self):
        return self.write({'state': 'pending', 'color':3})

    @api.multi
    def set_open(self):
        if self.env.context.get('first_open'):
            self.env['project.task'].create({})
        return self.write({'state': 'open','color': 6})