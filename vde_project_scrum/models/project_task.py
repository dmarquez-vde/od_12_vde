# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
#from bs4 import BeautifulSoup
from odoo.tools import float_is_zero, float_compare
import re
import time
from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
import logging
_logger = logging.getLogger(__name__)

#class ProjectTaskType(models.Model):
#    _inherit = "project.task.type"


#    legend_pending = fields.Char(
#        'Yellow Kanban Label', default=lambda s: _('Pendiente'), translate=True, required=True,
#        help='Override the default value displayed for the pending state for kanban selection, when the task or issue is in that stage.')


class ProjectTask(models.Model):
    _inherit = "project.task"
    _order = "sequence"

#    kanban_state = fields.Selection(selection_add=[('pending','Pendiente')])
#    legend_pending = fields.Char(related='stage_id.legend_pending', string='Kanban Valid Explanation', readonly=True, related_sudo=False)
    #@api.multi
    #def _getDuracion(self):
    #    self.duracion=self.date - self.date_start
    #    print "esto es duracion ", self.date - self.date_start

    #user_id = fields.Many2one('res.users', 'Assigned to', select=True, track_visibility='onchange', default="")
    #actor_ids = fields.Many2many(comodel_name='project.scrum.actors', string = 'Actor')
    #sprint_id = fields.Many2one(comodel_name = 'project.scrum.sprint', string = 'Sprint')
    #us_id = fields.Many2one(comodel_name = 'project.scrum.us', string = 'User Stories')
    #date_start = fields.Date(string = 'Starting Date', required=False, default=date.today())
    #date_end = fields.Date(string = 'Ending Date', required=False)
    #use_scrum = fields.Boolean(related='project_id.use_scrum')
    #description = fields.Html('Description')
    #current_sprint = fields.Boolean(compute='_current_sprint', string='Current Sprint', search='_search_current_sprint')
    #duracion = fields.Integer(compute="_getDuracion", string="Duracion", store=True)
    state=fields.Selection([('draft','Nueva'),
                                   ('open','En proceso'),
                                   ('cancelled', 'Cancelada'),
                                   ('pending','Pendiente'),
                                   ('validation','Validacion'),
                                   ('close','Cerrada')],
                                  'Status', required=True, copy=False, default='pending')
    #correcciones_count = fields.Integer(
    #    string='Correcciones', compute='_count_correccion_input',
    #    store=True)
    #correccion_id = fields.One2many(
    #    comodel_name='project.scrum.test', inverse_name='task_id',
    #    string='Correcciones')
    priority = fields.Selection(selection_add=[('3', 'Urgente')])
    sprint_id = fields.Many2one('project.sprint', string="Sprint")
    task_type = fields.Selection(string="Tipo", selection=[('backlog','Tarea'),('release', 'Entrega')], default="backlog")
    issue_ids = fields.One2many('project.issue', 'task_id', string="Incidencias")
    issue_count = fields.Integer(string="Incidencias", compute='_compute_issue_count')

    @api.multi
    def _compute_issue_count(self):
        for task in self:
            task.issue_count = len(task.issue_ids)

    #@api.one
    #@api.depends('correccion_id')
    #def _count_correccion_input(self):
    #    self.correcciones_count = len(self.correccion_id)

    @api.multi
    def set_done(self):
        return self.write({'state': 'close','color': 10, 'kanban_state':'done'})

    @api.multi
    def set_cancel(self):
        return self.write({'state': 'cancelled', 'color':1})

    @api.multi
    def set_pending(self):
        return self.write({'state': 'pending', 'color':3})

    @api.multi
    def set_validation(self):
        return self.write({'state': 'validation', 'color':8})

    @api.multi
    def set_open(self):
        return self.write({'state': 'open','color': 4})

    @api.multi
    def reset_project(self):
        return self.setActive()

    @api.model
    def create(self,vals):
        vals.update({'color':3})
        task_rec = super(ProjectTask,self).create(vals)
        return task_rec

    #@api.depends('sprint_id')
    #@api.one
    #def _current_sprint(self):
    #    sprint = self.env['project.scrum.sprint'].get_current_sprint(self.project_id.id)
    #    #~ _logger.error('Task computed %r' % self)
    #    if sprint:
    #        self.current_sprint = sprint.id == self.sprint_id.id
    #    else:
    #        self.current_sprint = False

    #def _search_current_sprint(self, operator, value):
        #~ raise Warning('operator %s value %s' % (operator, value))
    #    project_id = self.env.context.get('default_project_id', None)
    ##    sprint = self.env['project.scrum.sprint'].get_current_sprint(project_id)
    #    #~ raise Warning('sprint %s csprint %s context %s' % (self.sprint_id, sprint, self.env.context))
    #    _logger.error('Task %r' % self)
    #    return [('sprint_id', '=', sprint and sprint.id or None)]



    #@api.multi
    #def write(self, vals):
        #if vals.get('stage_id') == self.env.ref('project.project_tt_deployment').id:
            #vals['date_end'] = fields.datetime.now()
        #return super(ProjectTask, self).write(vals)

    #@api.model
    #def _read_group_sprint_id(self, present_ids, domain, **kwargs):
    #    project = self.env['project.project'].browse(self._resolve_project_id_from_context())

    #    if project.use_scrum:
    #        sprints = self.env['project.scrum.sprint'].search([('project_id', '=', project.id)], order='sequence').name_get()
    #        return sprints, None
    #    else:
    #        return [], None

    #@api.model
    #def _read_group_us_id(self, present_ids, domain, **kwargs):
    #    project = self.env['project.project'].browse(self._resolve_project_id_from_context())

    #    if project.use_scrum:
    #        user_stories = self.env['project.scrum.us'].search([('project_id', '=', project.id)], order='sequence').name_get()
    #        return user_stories, None
    #    else:
    #        return [], None

    """
    def _read_group_us_id(self, cr, uid, domain, read_group_order=None, access_rights_uid=None, context=None):
       # if self.use_scrum:
        us_obj = self.pool.get('project.scrum.us')
        order = us_obj._order
        access_rights_uid = access_rights_uid or uid
        if read_group_order == 'us_id desc':
            order = '%s desc' % order
        search_domain = []
        project_id = self._resolve_project_id_from_context(cr, uid, context=context)
        if project_id:
            search_domain += ['|', ('project_ids', '=', project_id)]
        search_domain += [('id', 'in', ids)]
        us_ids = us_obj._search(cr, uid, search_domain, order=order, access_rights_uid=access_rights_uid, context=context)
        result = us_obj.name_get(cr, access_rights_uid, us_ids, context=context)
        result.sort(lambda x,y: cmp(us_ids.index(x[0]), us_ids.index(y[0])))

        fold = {}
        for us in us_obj.browse(cr, access_rights_uid, us_ids, context=context):
            fold[us.id] = us.fold or False
        return result, fold
        #else:
          #  return [], None"""

    #def _auto_init(self, cr, context=None):
        #self._group_by_full['sprint_id'] = _read_group_sprint_id
        #self._group_by_full['us_id'] = _read_group_us_id
        #super(project_task, self)._auto_init(cr, context)

    #def _read_group_stage_ids(self, domain, read_group_order=None, access_rights_uid=None):
        #stage_obj = self.env.get['project.task.type']
        #order = stage_obj._order
        #access_rights_uid = access_rights_uid or self.env.uid
        #if read_group_order == 'stage_id desc':
            #order = '%s desc' % order
        #search_domain = []
        #project_id = self._resolve_project_id_from_context()
        #if project_id:
            #search_domain += ['|', ('project_ids', '=', project_id)]
        #search_domain += [('id', 'in', self.id)]
        #stage_ids = stage_obj._search(search_domain, order=order, access_rights_uid=access_rights_uid)
        #result = stage_obj.name_get()
        ## restore order of the search
        #result.sort(lambda x,y: cmp(stage_ids.index(x[0]), stage_ids.index(y[0])))

        #fold = {}
        #for stage in stage_obj.browse(cr, access_rights_uid, stage_ids, context=context):
            #fold[stage.id] = stage.fold or False
        #return result, fold

    #def _read_group_stage_ids(self, cr, uid, ids, domain, read_group_order=None, access_rights_uid=None, context=None):
        #stage_obj = self.pool.get('project.task.type')
        #order = stage_obj._order
        #access_rights_uid = access_rights_uid or uid
        #if read_group_order == 'stage_id desc':
            #order = '%s desc' % order
        #search_domain = []
        #project_id = self._resolve_project_id_from_context(cr, uid, context=context)
        #if project_id:
            #search_domain += ['|', ('project_ids', '=', project_id)]
        #search_domain += [('id', 'in', ids)]
        #stage_ids = stage_obj._search(cr, uid, search_domain, order=order, access_rights_uid=access_rights_uid, context=context)
        #result = stage_obj.name_get(cr, access_rights_uid, stage_ids, context=context)
        ## restore order of the search
        #result.sort(lambda x,y: cmp(stage_ids.index(x[0]), stage_ids.index(y[0])))

        #fold = {}
        #for stage in stage_obj.browse(cr, access_rights_uid, stage_ids, context=context):
            #fold[stage.id] = stage.fold or False
        #return result, fold

    #def _read_group_user_id(self, cr, uid, ids, domain, read_group_order=None, access_rights_uid=None, context=None):
        #res_users = self.pool.get('res.users')
        #project_id = self._resolve_project_id_from_context(cr, uid, context=context)
        #access_rights_uid = access_rights_uid or uid
        #if project_id:
            #ids += self.pool.get('project.project').read(cr, access_rights_uid, project_id, ['members'], context=context)['members']
            #order = res_users._order
            ## lame way to allow reverting search, should just work in the trivial case
            #if read_group_order == 'user_id desc':
                #order = '%s desc' % order
            ## de-duplicate and apply search order
            #ids = res_users._search(cr, uid, [('id','in',ids)], order=order, access_rights_uid=access_rights_uid, context=context)
        #result = res_users.name_get(cr, access_rights_uid, ids, context=context)
        ## restore order of the search
        #result.sort(lambda x,y: cmp(ids.index(x[0]), ids.index(y[0])))
        #return result, {}

#    try:
        #group_by_full['sprint_id'] = _read_group_sprint_id
        #_group_by_full['us_id'] = _read_group_us_id

#        _group_by_full = {
            #'sprint_id': _read_group_sprint_id,
            #'us_id': _read_group_us_id,
            #'stage_id': _read_group_stage_ids,
            #'user_id': _read_group_user_id,
#        }
#    except:
#        _group_by_full = {
            #'sprint_id': _read_group_sprint_id,
            #'us_id': _read_group_us_id,
            #'stage_id': _read_group_stage_ids,
            #'user_id': _read_group_user_id,
#        }
