# -*- coding: utf-8 -*-
##############################################################################
#
#
#
##############################################################################

{
    'name': 'VDE Project Scrum',
    'version': '10.0.1',
    'category': 'Project Management',
    'description': """
Using Scrum to plan the work in a team.
=========================================================================================================

More information:
    """,
    'author': 'VDE Suite',
    'website': 'http://www.vde-suite.com',
    'depends': ['project', 'mail', 'project_issue', 'project_team'],
    'data': [
        'data/ir_sequence_data.xml',
        'views/project_scrum_view.xml',
        'views/project_project.xml',
        'views/project_task.xml',
        'views/project_issue.xml',
        'wizard/project_scrum_test_task_view.xml',
        'security/ir.model.access.csv',
        'security/project_security.xml',
       ],
    #'external_dependencies': {
        #'python' : ['bs4'],
    #},
    #'demo': ['data/project_scrum_demo.xml'],
    'installable': True,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
