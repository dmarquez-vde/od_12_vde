# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
#from bs4 import BeautifulSoup
from odoo.tools import float_is_zero, float_compare
import re
import time
from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
import logging
_logger = logging.getLogger(__name__)

class ProjectApp(models.Model):

    _name = 'project.app'

    name = fields.Char(string="Nombre")
    color = fields.Integer(string='Color Index')


class ProjectProject(models.Model):
    _inherit = 'project.project'

    sprint_ids = fields.One2many(comodel_name = "project.scrum.sprint", inverse_name = "project_id", string = "Entregas")
    user_story_ids = fields.One2many(comodel_name = "project.scrum.us", inverse_name = "project_id", string = "User Stories")
    meeting_ids = fields.One2many(comodel_name = "project.scrum.meeting", inverse_name = "project_id", string = "Meetings")
    test_case_ids = fields.One2many(comodel_name = "project.scrum.test", inverse_name = "project_id", string = "Correcciones")
    sprint_count = fields.Integer(compute = '_sprint_count', string="Entregas")
    user_story_count = fields.Integer(compute = '_user_story_count', string="User Stories")
    meeting_count = fields.Integer(compute = '_meeting_count', string="Meetings")
    test_case_count = fields.Integer(compute = '_test_case_count', string="Correcciones")
    use_scrum = fields.Boolean(store=True)
    default_sprintduration = fields.Integer(string = 'Calendar', required=False, default=14,help="Default Sprint time for this project, in days")
    manhours = fields.Integer(string = 'Man Hours', required=False,help="How many hours you expect this project needs before it's finished")
    tipo = fields.Selection([('odoo','Odoo'),
                                   ('vtiger','Vtiger'),
                                   ('elastix','Elastix'),
                                   ('nexus','Nexus'),
                                   ('desarrollo', 'Desarrollo a medida'),
                                   ('integracion','Integracion'),
                                   ('diagnostico','Diagnostico'),
                                   ],
                                  'Tipo', required=True, copy=False)
    duracion = fields.Integer(compute="_getDuracion", string="Duracion", store=True)
    state=fields.Selection([('draft','Nuevo'),
                                   ('open','Abierto'),
                                   ('cancelled', 'Cancelado'),
                                   ('pending','Pendiente'),
                                   ('close','Cerrado')],
                                  'Status', required=True, copy=False, default='open')
    partner = fields.Char(string="Prospecto/Cliente")
    contact_id = fields.Many2one('res.partner', string="Contacto")
    comercial_id = fields.Many2one('res.users', string="Comercial")
    app_ids = fields.Many2many(comodel_name='project.app', relation='project_app_rel',
        column1='project_id', column2='app_id', string='Tasks Stages',)


    def _sprint_count(self):    # method that calculate how many sprints exist
        for p in self:
            p.sprint_count = len(p.sprint_ids)

    def _user_story_count(self):    # method that calculate how many user stories exist
        for p in self:
            p.user_story_count = len(p.user_story_ids)

    def _meeting_count(self):    # method that calculate how many meetings exist
        for p in self:
            p.meeting_count = len(p.meeting_ids)

    def _test_case_count(self):    # method that calculate how many test cases exist
        for p in self:
            p.test_case_count = len(p.test_case_ids)

    @api.multi
    def _getDuracion(self):
        self.duracion=self.date - self.date_start
        print "esto es duracion ", self.date - self.date_start

    @api.multi
    def set_done(self):
        return self.write({'state': 'close','color': 5})

    @api.multi
    def set_cancel(self):
        return self.write({'state': 'cancelled', 'color':2})

    @api.multi
    def set_pending(self):
        return self.write({'state': 'pending', 'color':3})

    @api.multi
    def set_open(self):
        return self.write({'state': 'open','color': 6})

    @api.model
    def create(self,vals):
        print "entra a create ", vals.get('tipo')
        vals.update({'color':3})
        vals.update({'state':'pending'})
        vals.update({'code':self.env['ir.sequence'].next_by_code('project.project')})
        if vals.get('tipo')=='diagnostico':
            ids = self.env['project.task.type'].search([('case_default','=',True),('tipo','=','diagnostico')])
        else:
            ids = self.env['project.task.type'].search([
                '|',
                '&', ('case_default', '=', True),('tipo','=',vals.get('tipo')), '&', ('case_default', '=', True),('tipo','=','all')
                ])
        print "esto es ids1 ", ids
        if not ids:
            print "not ids"
            ids = self.env['project.task.type'].search([
            ('case_default', '=', True),('tipo','=','all')])
        print "esto es ids2 ", ids
        stages = []
        for idss in ids:
            stages.append(idss.id)
        vals.update({'type_ids': [(6, 0, stages)]})
        print "esto es vals ", vals
        task_rec = super(ProjectProject,self).create(vals)
        return task_rec

    def _get_type_common(self):
        print "esto es context ", self.env.context
        print "esto es tipo ", self.tipo
        ids = self.env['project.task.type'].search([
            ('case_default', '=', True),('tipo','=',self.tipo)])
        if not ids:
            print "not ids"
            ids = self.env['project.task.type'].search([
            ('case_default', '=', True),('tipo','=','all')])
        print "esto es ids ", ids
        return ids

    type_ids = fields.Many2many(
        comodel_name='project.task.type', relation='project_task_type_rel',
        column1='project_id', column2='type_id', string='Tasks Stages',
        #default=_get_type_common
    )

class ProjectTaskType(models.Model):
    _inherit = 'project.task.type'

    case_default = fields.Boolean(
        string='Default for New Projects',
        help='If you check this field, this stage will be proposed by default '
             'on each new project. It will not assign this stage to existing '
             'projects.')
    tipo = fields.Selection([('odoo', 'Odoo'),
                                   ('vtiger', 'Vtiger'),
                                   ('elastix', 'Elastix'),
                                   ('nexus', 'Nexus'),
                                   ('desarrollo', 'Desarrollo a medida'),
                                   ('integracion', 'Integracion'),
                                   ('diagnostico', 'Diagnostico'),
                                   ('all', 'Todos')
                                   ],
                                  'Tipo', copy=False, default='all')