# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
#from bs4 import BeautifulSoup
from odoo.tools import float_is_zero, float_compare
import re
import time
from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
import logging
_logger = logging.getLogger(__name__)

class ProjectIssue(models.Model):
    _inherit="project.issue"

    involucrados=fields.Selection([('cliente','Cliente'),
                                       ('vde','VDE-Desarrollo'),
                                       ('cliente-vde', 'Cliente y VDE'),
                                       ],
                                      'Involucrados', required=True, copy=False)
    asunto=fields.Selection([('requerimiento','Nuevo requerimiento'),
                                   ('retraso','Retraso'),
                                   ('modificacion', 'Modificacion'),
                                   ],
                                  'Asunto', required=True, copy=False)
    fecha_aviso=fields.Date('Fecha de aviso')
    fecha_entrega=fields.Date('Fecha de entrega')
    a_tiempo=fields.Boolean('Entrega a tiepo?')
    state=fields.Selection([('draft','Nueva'),
                                   ('open','Abierta'),
                                   ('cancelled', 'Cancelada'),
                                   #('pending','Pendiente'),
                                   ('close','Cerrada')],
                                  'Status', required=True, copy=False, default='open')

    @api.model
    def create(self,vals):
        vals.update({'color':6})
        task_rec = super(ProjectIssue,self).create(vals)
        return task_rec

    @api.multi
    def set_done(self):
        return self.write({'state': 'close','color': 5})

    @api.multi
    def set_cancel(self):
        return self.write({'state': 'cancelled', 'color':2})

    @api.multi
    def set_pending(self):
        return self.write({'state': 'pending', 'color':3})

    @api.multi
    def set_open(self):
        return self.write({'state': 'open','color': 6})

    @api.multi
    def reset_project(self):
        return self.setActive()
