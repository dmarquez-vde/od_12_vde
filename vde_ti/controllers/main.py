# -*- coding: utf-8 -*-
###############################################################################
#
#    VDE Suite
#    Copyright (C) 2009-TODAY VDE Suite(<https://www.vde-suite.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from odoo import http
#from odoo.http import request
from odoo.http import content_disposition, Controller, request, route
from odoo.addons.website_form.controllers.main import WebsiteForm

class WebsiteForm(WebsiteForm):

    @route('/ws', type='http', auth='public', website=True)
    def ws(self, **kargs):
        values = {}
        #convocatorias = request.env['op.admission.register'].sudo().search([('state','=','application')])
        #countries = request.env['res.country'].sudo().search([])
        #values.update({'register_ids':convocatorias})
        #values.update({'countries':countries})
        response = request.render("vde_ti.vde_ti_ws_request", values)
        response.headers['X-Frame-Options'] = 'DENY'
        return response

    @route('/ws/create', type='http', auth='public', website=True)
    def ws_create(self, **kargs):
        values = {}
        errors = {}
        for field_name, field_value in kargs.items():
            values[field_name] = field_value
        print ("Values ", values)
        user = request.env['res.users'].sudo().search([('login','=',values['token_master'])])
        if not user:
            errors['error'] = "El token no eixste"
            return http.request.render("vde_ti.vde_ti_ws_error", errors)
        #if len(values['curp'])!=18:
        #    errors['error'] = "La longitud CURP: "+values['curp']+" es diferente de 18 caracteres."
        #    return http.request.render("vde_ti.ws_error", errors)
        #aspirante = request.env['op.admission'].sudo().search([('curp','=',values['curp']),('register_id','=',int(values['register_id']))])
        #if aspirante:
            #return "Lo sentimos, ya existe un aspirante registrado con el CURP: "+str(values['curp'])
            #errors['curp_error'] = values['curp']
        #    errors['error'] = "El CURP: "+values['curp']+" ya ha sido registrado previamente."
        #    return http.request.render("vde_ti.ws_error", errors)
        #else:
        #    convocatoria = request.env['op.admission.register'].sudo().search([('id','=',int(values['register_id']))])

