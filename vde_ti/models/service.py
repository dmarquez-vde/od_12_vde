# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-Today Acespritech Solutions Pvt Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields,models,api,_
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning
from odoo.exceptions import UserError, AccessError
from odoo import _
import odoo.addons.decimal_precision as dp

class itService(models.Model):

    _name = 'it.service'
    _inherit = ['mail.thread']

    name = fields.Char(string="Nombre")
    url = fields.Char(string="URL")
    user = fields.Char(string="Usuario")
    password = fields.Char(string="Password")
    tipo = fields.Selection([
        ('hosting', 'Hosting'),
        ('vps', 'VPS'),
        ('pac', 'PAC'),
        ('paypal', 'Paypal'),
        ('mayorista', 'Mayorista'),
        ('otro', 'Otro'),
        ])
    domain_ids = fields.One2many('it.domain', 'service_id', string="Dominios")
    state = fields.Selection([
        ('active', 'Activa'),
        ('inactive', 'Inactiva'),
        ],default="active")