# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-Today Acespritech Solutions Pvt Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields,models,api,_
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning
from odoo.exceptions import UserError, AccessError
from odoo import _
import odoo.addons.decimal_precision as dp

class tiIp(models.Model):

    _name = 'it.ip'
    _inherit = ['mail.thread']
    _sql_constraints = [
        ('it_ip_unique_name', 'unique (name)','La ip debe ser unica!')]

    name = fields.Char(string="IP")
    netmask_id = fields.Many2one(string="Netmask", related="subnet_id.netmask_id")
    note = fields.Char(string="Descripcion")
    section_id = fields.Many2one('it.section', string="Seccion", related="subnet_id.section_id")
    subnet_id = fields.Many2one('it.subnet', string="Subnet")
    state = fields.Selection([
        ('unused', 'Libre'),
        ('used', 'Asignada')
        ],default='unused')