# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-Today Acespritech Solutions Pvt Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

##############################################################################

from openerp import fields,models,api,_
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning, ValidationError
from odoo.exceptions import UserError, AccessError
from odoo import _
import odoo.addons.decimal_precision as dp
import urllib.request

class tiCron(models.Model):

    _name = 'it.cron'
    _inherit = ['mail.thread']


    name = fields.Char(string="Descripcion")
    path = fields.Char(string="Path")
    active = fields.Boolean(string="Activo", default=True)
    cron_id = fields.Many2one('ir.cron', string="Cron")
    nextcall = fields.Datetime(string="Siguiente ejecucion", related="cron_id.nextcall")
    x = fields.Integer(string="Ejecutar cada", related="cron_id.interval_number")
    y = fields.Selection(related="cron_id.interval_type")

    @api.multi
    def action_create_cron(self):
        if self.cron_id:
            raise ValidationError('Ya existe un cron de odoo asociado a este registro.')
        else:
            class_name = self.__class__.__name__
            local_context = self.env.context.copy()
            model = self.env['ir.model'].search([('model','=',class_name)])
            code = 'model.run_cron('+str(self.id)+')'
            cron_ids = self.env['ir.cron'].create({'name': 'VDE Cron: '+self.name, 'model_id':model.id, 'state':'code', 'numbercall':-1, 'priority':5, 'user_id':1, 'code':code, 'interval_numer':10, 'interval_type':'minutes'})
            if cron_ids:
                self.write({'cron_id':cron_ids.id})
            else:
                raise ValidationError('Ocurrio un error al crear el cron.')

    @api.multi
    def action_execute_manual(self):
        self.cron_id.method_direct_trigger()

    @api.model
    def run_cron(self, cron_id):
        print ("cron_id ", cron_id)
        cron_id = self.env['it.cron'].search([('id','=',cron_id)])
        print ("cron_ids ", cron_id)
        if cron_id:
            print ("path ", cron_id.path)
            urllib.request.urlopen(cron_id.path)

    @api.multi
    def unlink(self):
        for record in self:
            if record.cron_id:
                if record.cron_id.unlink():
                    return super(tiCron, self).unlink()

