# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-Today Acespritech Solutions Pvt Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields,models,api,_
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning
from odoo.exceptions import UserError, AccessError
from odoo import _
import odoo.addons.decimal_precision as dp
#from netaddr import IPNetwork
from ipaddress import IPv4Network
from odoo.exceptions import UserError, AccessError

class tiSubnet(models.Model):

    _name = 'it.subnet'
    _inherit = ['mail.thread']

    _sql_constraints = [
         ('it_subnet_unique_name', 'unique (name)','El nombre de la red debe ser unico!')]

    def create_ips(self):
        if len(self.ip_ids) > 0:
            raise UserError(_('Ya existen ips creadas.') )
        else:
            ip = self.name + "/" + str(self.netmask_id.code)
            #for ip in IPNetwork(ip):
            for ip in IPv4Network(ip):
                print ('%s' % ip)
                vals = {'name': ip, 'subnet_id': self.id,'state':'unused'}
                ip_id = self.env['it.ip'].create(vals)
                if not ip_id:
                    raise UserError(_('Error al crear la IP.'+str(ip)) )

    name = fields.Char(string="Nombre")
    netmask_id = fields.Many2one('it.netmask', string="Netmask")
    note = fields.Char(string="Descripcion")
    section_id = fields.Many2one('it.section', string="Seccion")
    vlan_id = fields.Many2one('it.vlan', string="Vlan")
    ip_ids = fields.One2many('it.ip', 'subnet_id', string="IPs")
    network_id = fields.Many2one('it.ip', string="Network")
    gateway_id = fields.Many2one('it.ip', string="Gateway")
    broadcast_id = fields.Many2one('it.ip', string="Broadcast")

    @api.multi
    def write(self,vals):
        print ("esto es vals write subnet ", vals)
        print ("esto es vals write subnet", self.network_id.id)
        network = self.network_id.id
        gateway = self.gateway_id.id
        broadcast = self.broadcast_id.id
        #vals.update({'user_id':self._uid})
        #vals.update({'remaning_qty':vals.get('qty')})
        #sequence = self.env['ir.sequence'].get('pocontract')
        #vals['name']=sequence
        result =  super(models.Model, self).write(vals)
        if result:
            if vals.get('network_id'):
                self.env['it.ip'].browse(vals.get('network_id')).write({'state':'used'})
            if vals.get('gateway_id'):
                self.env['it.ip'].browse(vals.get('gateway_id')).write({'state':'used'})
            if vals.get('broadcast_id'):
                self.env['it.ip'].browse(vals.get('broadcast_id')).write({'state':'used'})
            if vals.get('network_id') == False:
                self.env['it.ip'].browse(network).write({'state': 'unused'})
            if vals.get('gateway_id') == False:
                self.env['it.ip'].browse(gateway).write({'state': 'unused'})
            if vals.get('broadcast_id') == False:
                self.env['it.ip'].browse(broadcast).write({'state': 'unused'})
        return result
