# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-Today Acespritech Solutions Pvt Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields,models,api,_
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning
from odoo.exceptions import UserError, AccessError
from odoo import _
import odoo.addons.decimal_precision as dp

class tiHostPort(models.Model):

    _name = 'it.host.port'

    name = fields.Char(string="Servicio")
    protocol = fields.Selection([('tcp','TCP'),('udp','UDP')], string="Protocolo")
    port_e = fields.Integer(string="Puerto externo")
    port_i = fields.Integer(string="Puerto interno")
    ip = fields.Many2one('it.ip', string="IP Privada")
    host_id = fields.Many2one('it.host', string="Equipo")

class tiHost(models.Model):

    _name = 'it.host'
    _inherit = ['mail.thread']
    #_sql_constraints = [
        #('it_ip_unique_name', 'unique (name)','La ip debe ser unica!')]

    name = fields.Char(string="Nombre")
    tipo = fields.Selection([
        (1, 'Servidor'),
        (2, 'Laptop'),
        (3, 'Desktop'),
        (4, 'Terminal'),
        (5, 'PBX'),
        (6, 'Telefono IP'),
        (7, 'Router'),
        (8, 'Switch'),
        (9, 'Accesspoint'),
        ])
    desc = fields.Char(string="Uso IP principal")
    ip_id = fields.Many2one('it.ip', string="IP princial")
    section_id = fields.Many2one('it.section', string="Seccion", related="ip_id.section_id")
    subnet_id = fields.Many2one('it.subnet', string="Subnet", related="ip_id.subnet_id")
    ip_id1 = fields.Many2one('it.ip', string="IP 1")
    section_id1 = fields.Many2one('it.section', string="Seccion", related="ip_id1.section_id")
    subnet_id1 = fields.Many2one('it.subnet', string="Subnet", related="ip_id1.subnet_id")
    user1 = fields.Char(string="User")
    pwd1 = fields.Char(string="Password")
    desc1 = fields.Char(string="Uso IP 1")
    ip_id2 = fields.Many2one('it.ip', string="IP 2")
    section_id2 = fields.Many2one('it.section', string="Seccion", related="ip_id2.section_id")
    subnet_id2 = fields.Many2one('it.subnet', string="Subnet", related="ip_id2.subnet_id")
    user2 = fields.Char(string="User")
    pwd2 = fields.Char(string="Password")
    desc2 = fields.Char(string="Uso IP 2")
    partner_id = fields.Many2one('res.partner', string="Cliente", domain="[('customer','=',True)]")
    marca = fields.Char(string="Marca")
    vm_ids = fields.One2many('it.vm', 'host_id', string="VMs")
    user = fields.Char(string="User")
    pwd = fields.Char(string="Password")
    wifi_ids = fields.One2many('it.wifi', 'host_id', string="Wifi")
    virtual = fields.Boolean(string="Virtualizado?")
    server_so = fields.Char(string="SO")
    server_cpu = fields.Char(string="CPU")
    server_ram = fields.Integer(string="GB RAM")
    server_storage = fields.Integer(string="TB Storage")
    state = fields.Selection([
        ('unused', 'Libre'),
        ('used', 'Asignado')
        ],default="unused")
    port_ids = fields.One2many('it.host.port', 'host_id', string="Puertos")

    @api.multi
    def write(self, vals):
        old_ip = self.ip_id
        old_ip1 = self.ip_id1
        old_ip2 = self.ip_id2
        result = super(models.Model, self).write(vals)
        if result:
            if vals.get('ip_id'):
                old_ip.write({'state': 'unused'})
                self.env['it.ip'].browse(vals.get('ip_id')).write({'state': 'used'})
            if vals.get('ip_id1'):
                old_ip1.write({'state': 'unused'})
                self.env['it.ip'].browse(vals.get('ip_id1')).write({'state': 'used'})
            if vals.get('ip_id2'):
                old_ip2.write({'state': 'unused'})
                self.env['it.ip'].browse(vals.get('ip_id2')).write({'state': 'used'})
        return result

    @api.model
    def create(self, vals):
        #print "entra create"
        ip = self.env['it.ip'].browse(vals.get('ip_id'))
        #print "esto es ip ", ip
        ip.write({'state': 'used'})
        if vals.get('ip_id1'):
            ip = self.env['it.ip'].browse(vals.get('ip_id1'))
            #print "esto es ip ", ip
            ip.write({'state': 'used'})
        if vals.get('ip_id2'):
            ip = self.env['it.ip'].browse(vals.get('ip_id2'))
            #print "esto es ip ", ip
            ip.write({'state': 'used'})
        host = super(models.Model, self).create(vals)
        return host



    #netmask_id = fields.Many2one(string="Netmask", related="subnet_id.netmask_id")
    #note = fields.Char(string="Descripcion")

    #subnet_id = fields.Many2one('it.subnet', string="Subnet")
    #state = fields.Selection([
        #('unused', 'Libre'),
        #('used', 'Asignada')
        #])